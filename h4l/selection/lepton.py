
from __future__ import annotations

from columnflow.selection import Selector, SelectionResult, selector
from columnflow.util import maybe_import, dev_sandbox
from h4l.util import IF_NANO_V9, IF_NANO_V10

np = maybe_import("numpy")
ak = maybe_import("awkward")


@selector(
    uses=(
        {
            f"Electron.{var}"
            for var in {
                "pt", "eta",
            }
        }
    ),
    exposed=False,
    sandbox=dev_sandbox("bash::$CF_BASE/sandboxes/venv_columnar.sh"),
)
def electron_selection(
    self: Selector,
    events: ak.Array,
    **kwargs,
):
    min_pt = 15
    pt = events.Electron.pt
    
    # mask for selecting electrons
    electron_mask = (
        (pt > min_pt) &
        (abs(events.Electron.eta) < 2.5)
    )

    # select and sort electrons
    sorted_idx = ak.argsort(events.Electron.pt, axis=1, ascending=False)
    selected_electron_idx = sorted_idx[electron_mask[sorted_idx]]

    return events, SelectionResult(
        objects={
            "Electron": {
                "Electron": selected_electron_idx,
            },
        },
    )


@selector(
    uses=({
        f"Muon.{var}" for var in [
            # four momenta information
            "pt", "eta", "phi", "mass",
        ]
    }),
    exposed=False,
)
def muon_selection(
    self: Selector,
    events: ak.Array,
    **kwargs,
) -> tuple[ak.Array, SelectionResult]:
    min_pt = 15
    max_eta = 2.4
    selected_muon_mask = (
        (events.Muon.pt > min_pt) &
        (abs(events.Muon.eta) < max_eta)
    )
    sorted_idx = ak.argsort(events.Muon.pt, axis=1, ascending=False)

    # filter unselected muon indices
    muon_idx = sorted_idx[selected_muon_mask[sorted_idx]]

    return events, SelectionResult(
        objects={
            "Muon": {
                "Muon": muon_idx,
            },
        },
    )
